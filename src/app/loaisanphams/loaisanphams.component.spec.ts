import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoaisanphamsComponent } from './loaisanphams.component';

describe('LoaisanphamsComponent', () => {
  let component: LoaisanphamsComponent;
  let fixture: ComponentFixture<LoaisanphamsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoaisanphamsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoaisanphamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
