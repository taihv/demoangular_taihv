import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { MessageService, ConfirmationService } from 'primeng/api';

import { Customer, Productmanagement, Producttype, Productgroup, Representative, Message } from 'src/app/demo/api/customer';
import { Product } from 'src/app/demo/api/product';
import { CustomerService } from 'src/app/demo/service/customer.service';
import { ProductService } from 'src/app/demo/service/product.service';

import {DialogAddsanphamComponent} from './modals/dialog-addsanpham/dialog-addsanpham.component';
import {DialogEditsanphamComponent} from './modals/dialog-editsanpham/dialog-editsanpham.component';
import {DialogViewsanphamComponent} from './modals/dialog-viewsanpham/dialog-viewsanpham.component';

import {DialogDelmessageComponent} from 'src/app/showmodals/dialog-delmessage/dialog-delmessage.component';



import {DialogService} from 'primeng/dynamicdialog';
import {DynamicDialogRef} from 'primeng/dynamicdialog';


interface expandedRows {
    [key: string]: boolean;
}


@Component({
  selector: 'app-sanphams',
  templateUrl: './sanphams.component.html',
  styleUrls: ['./sanphams.component.scss'],
  providers: [DialogService, MessageService]
})


export class SanphamsComponent implements OnInit {

  public productmanagements: Productmanagement[] = [];
  public producttypes: Producttype[]= [];
  public productgroups: Productgroup[]= [];
  public selectedproducttype:  Producttype = {name: '', code:''};
  public selectedproductgroup: Productgroup = {name: '', code:''};

  // public igroup: Productgroup = {name: '', code:''};
  // public itype: Producttype = {name: '', code:''};
  
  public item: Productmanagement = {proGroup:this.selectedproductgroup, proType: this.selectedproducttype};
  public caption: string="";
  rowsPerPageOptions = [5, 10, 20];
  productDialog: boolean = false;
  submitted: boolean = false;
  
  
  constructor(public dialogService: DialogService, public messageService: MessageService) { }

  ngOnInit(): void {
    this.producttypes = [
      {name: 'Select', code: ''},
      {name: 'Glass material', code: '01'},
      {name: 'Plastic material', code: '02'},
      {name: 'Plastic material', code: '03'}];
    this.productgroups = [
      {name: 'Select', code: ''},
      {name: 'Product group with handles', code: '01'},
      {name: 'Plastic cup without lid', code: '02'},
      {name: 'Plastic cup with lid', code: '03'}];

      this.productmanagements = [
        {id:1,no:1,product_name: 'Glass Cup with smooth glass handle 450ml',proType: {name: 'Glass material', code: '01'},  proGroup: {name: 'Product group with handles', code:'01'}, product_dom: '01'},
        {id:2,no:2,product_name: 'Patterned glass handle cup material', proType: {name: 'Glass material',code: '01'}, proGroup: {name: 'Product group with handles', code:'01'},  product_dom: '02'},
        {id:3,no:3,product_name: 'Set of 35ml small plastic cups', proType: {name: 'Plastic material',code: '02'},proGroup: {name: 'Plastic cup without lid', code:'02'},  product_dom: '03'},
        {id:4,no:4,product_name: '400ml Japanese plastic cup', proType: {name: 'Plastic material',code: '02'},proGroup: {name: 'Plastic cup with lid', code:'03'},  product_dom: '04'}];

  }
  paginate(event: any) {
    //event.first = Index of the first record
    //event.rows = Number of rows to display in new page
    //event.page = Index of the new page
    //event.pageCount = Total number of pages
  }
  additem(item: any)
  {
    let ref: DynamicDialogRef;
    ref = this.dialogService.open(DialogAddsanphamComponent, {
      header: 'Add Product',
      width: '920px',
      contentStyle: {"overflow": "hidden"},
      baseZIndex: 10000,
      data: { productmanagements: this.productmanagements, producttypes: this.producttypes,productgroups: this.productgroups,item: item} 
    });
  

    ref.onClose.subscribe((res: any) =>{
        let i =0;
        this.productmanagements.forEach(element => {
          i++; 
          element.no = i;
        });

        if ((res != null) && (res.flag ==2))   {

          this.additem(res.item);
        }

        

    });
  }
  addProduct() {

    this.item =  {proGroup:this.selectedproductgroup, proType: this.selectedproducttype};
    this.additem(this.item);

  }
  editProduct(item: any) {

    
    let data = Object.assign({}, item); 
    let ref: DynamicDialogRef;
    ref = this.dialogService.open(DialogEditsanphamComponent, {
      header: 'Edit Product',
      width: '920px',
      contentStyle: {"overflow": "hidden"},
      baseZIndex: 10000,
      data: { productmanagements: this.productmanagements, producttypes: this.producttypes,productgroups: this.productgroups,item: data} 
    });
  

    ref.onClose.subscribe((res: any) =>{
        // if (res != null) {
          
        //   // this.item = this.productmanagements.filter(it => it.no === res.no)[0];
        //   // if (item != null)
        //   // {
        //   //   // this.item = res;

        //   //    this.item.product_name = res.product_name;

        //   //   this.item.proGroup = item.proGroup;
        //   //   this.item.proGroup_id = item.product_name;
        //   //   this.item.proGroup_name = item.product_name;
    
        //   //   this.item.proType = item.proType;


        //   //   this.item.proType_id = item.proType_id;
        //   //   this.item.proType_name = item.proType_name;
        //   //   this.item.product_name = item.product_name;
        //   //   this.item.unit_product = item.unit_product;
            
        //   //   this.item.material = item.material;
        //   //   this.item.weight = item.weight;
        //   //   this.item.capacity = item.capacity;
        //   //   this.item.description = item.description;
            
        //   // } else {
        //   //   this.productmanagements.push(item);
        //   // }

          

        // }

        if ((res != null) && (res.flag ==2))   {

          this.additem(res.item);
        }
    });


  }
  
  viewProduct(item: any) {

    
    let data = Object.assign({}, item); 
    let ref: DynamicDialogRef;
    ref = this.dialogService.open(DialogViewsanphamComponent, {
      header: 'View Product',
      width: '920px',
      contentStyle: {"overflow": "hidden"},
      baseZIndex: 10000,
      data: { productmanagements: this.productmanagements, producttypes: this.producttypes,productgroups: this.productgroups,item: data} 
    });
  

    ref.onClose.subscribe((item: Productmanagement) =>{
    });


  }

  showdelmessage(item: Productmanagement) {

    
    let data: Message = {title:"Are you sure ?", message: "Do you want to delete product: ", name : item.product_name}; 

    let ref: DynamicDialogRef;
    ref = this.dialogService.open(DialogDelmessageComponent, {
      header: 'Confirm',
      width: '480px',
      contentStyle: {"overflow": "hidden"},
      baseZIndex: 10000,
      data: { item: data} 
    });
  

    ref.onClose.subscribe((res: Productmanagement) =>{
      if (res != null)
      {
        let items =  this.productmanagements.filter(it => it.no != item.no);
        this.productmanagements = Object.assign([], items); 
  
        let i =0;
        this.productmanagements.forEach(element => {
          i++; 
          element.no = i;
        });
      }
      


    });


  }

}
