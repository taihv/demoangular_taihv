import { Component, OnInit } from '@angular/core';
import { Customer, Productmanagement, Producttype, Productgroup, Representative,Message } from 'src/app/demo/api/customer';
import { Product } from 'src/app/demo/api/product';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { debug } from 'console';


@Component({
  selector: 'app-dialog-addsanpham',
  templateUrl: './dialog-addsanpham.component.html',
  styleUrls: ['./dialog-addsanpham.component.scss']
})
export class DialogAddsanphamComponent implements OnInit {

  public productmanagements: Productmanagement[] = [];
  public item: Productmanagement = {proGroup:{name: '', code:''}, proType: {name: '', code:''}};
  public producttypes: Producttype[]= [];
  public productgroups: Productgroup[]= [];
  submitted: boolean = false;

  public fOK: any= {};  
  

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  ngOnInit(): void {
    
    this.productmanagements =this.config.data.productmanagements;
    this.producttypes =this.config.data.producttypes;
    this.productgroups =this.config.data.productgroups;
    this.item =this.config.data.item;
    

  }
  saveProduct()
  {
    
    this.item.proGroup_name = this.item.proGroup.name;
    this.item.proType_name = this.item.proType.name;
    
    this.productmanagements.push (this.item);
    
    // this.ref.close(this.item);
    this.fOK= {flag:1, item: this.item };
    this.ref.close(this.fOK);
  }
  nexProduct ()
  {
    this.item.no = this.productmanagements.length +1;
    this.item.proGroup_name = this.item.proGroup.name;
    this.item.proType_name = this.item.proType.name;
    this.productmanagements.push (this.item);
    
    let it = Object.assign({}, this.item); 
    this.item =   {proGroup:{name: '', code:''}, proType: {name: '', code:''}}; 

    this.item.proGroup= it.proGroup;
    this.item.proType= it.proType;

    this.fOK= {flag:2, item: this.item };
    this.ref.close(this.fOK);
    
    
  }
  hideDialog()
  {
    this.fOK= {flag:0, item: null };

    this.ref.close(this.fOK);
    
  }

}
