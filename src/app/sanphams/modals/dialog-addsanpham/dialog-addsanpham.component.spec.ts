import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddsanphamComponent } from './dialog-addsanpham.component';

describe('DialogAddsanphamComponent', () => {
  let component: DialogAddsanphamComponent;
  let fixture: ComponentFixture<DialogAddsanphamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogAddsanphamComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DialogAddsanphamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
