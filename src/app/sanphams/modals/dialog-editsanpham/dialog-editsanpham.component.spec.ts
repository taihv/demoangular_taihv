import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEditsanphamComponent } from './dialog-editsanpham.component';

describe('DialogEditsanphamComponent', () => {
  let component: DialogEditsanphamComponent;
  let fixture: ComponentFixture<DialogEditsanphamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogEditsanphamComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DialogEditsanphamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
