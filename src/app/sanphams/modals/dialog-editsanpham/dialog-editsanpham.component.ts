import { Component, OnInit } from '@angular/core';
import { Customer, Productmanagement, Producttype, Productgroup, Representative } from 'src/app/demo/api/customer';
import { Product } from 'src/app/demo/api/product';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { debug } from 'console';

@Component({
  selector: 'app-dialog-editsanpham',
  templateUrl: './dialog-editsanpham.component.html',
  styleUrls: ['./dialog-editsanpham.component.scss']
})
export class DialogEditsanphamComponent implements OnInit {

  public productmanagements: Productmanagement[] = [];
  public item: Productmanagement = {proGroup:{name: '', code:''}, proType: {name: '', code:''}};
  public producttypes: Producttype[]= [];
  public productgroups: Productgroup[]= [];
  submitted: boolean = false;
  public fOK: any= {};  

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  ngOnInit(): void {
    debugger;
    this.productmanagements =this.config.data.productmanagements;
    this.producttypes =this.config.data.producttypes;
    this.productgroups =this.config.data.productgroups;
    this.item =this.config.data.item;

  }
  saveProduct()
  {
    // this.item.no = this.productmanagements.length +1;
    this.item.proGroup_name = this.item.proGroup.name;
    this.item.proType_name = this.item.proType.name;

    let itdata= this.productmanagements.filter(it => it.no === this.item.no)[0];
    if (itdata != null)
    {
      // itdata =   Object.assign({}, this.item); 
      itdata.product_name = this.item.product_name;
      itdata.proGroup = this.item.proGroup;
      itdata.proGroup_id = this.item.product_name;
      itdata.proGroup_name = this.item.product_name;

      itdata.proType = this.item.proType;


      itdata.proType_id = this.item.proType_id;
      itdata.proType_name = this.item.proType_name;
      itdata.product_name = this.item.product_name;
      itdata.unit_product = this.item.unit_product;
      
      itdata.material = this.item.material;
      itdata.weight = this.item.weight;
      itdata.capacity = this.item.capacity;
      itdata.description = this.item.description;

      
    } else {
      this.productmanagements.push(this.item);
    }

    this.fOK= {flag:1, item: this.item };
    this.ref.close(this.fOK);
  }
  nexProduct ()
  {

    this.item.proGroup_name = this.item.proGroup.name;
    this.item.proType_name = this.item.proType.name;
    let itdata= this.productmanagements.filter(it => it.no === this.item.no)[0];
    if (itdata != null)
    {
      itdata.product_name = this.item.product_name;
      itdata.proGroup = this.item.proGroup;
      itdata.proGroup_id = this.item.product_name;
      itdata.proGroup_name = this.item.product_name;

      itdata.proType = this.item.proType;


      itdata.proType_id = this.item.proType_id;
      itdata.proType_name = this.item.proType_name;
      itdata.product_name = this.item.product_name;
      itdata.unit_product = this.item.unit_product;
      
      itdata.material = this.item.material;
      itdata.weight = this.item.weight;
      itdata.capacity = this.item.capacity;
      itdata.description = this.item.description;
      
    } else {
      this.productmanagements.push (this.item);
    }


    let it = Object.assign({}, this.item); 
    this.item =   {proGroup:{name: '', code:''}, proType: {name: '', code:''}}; 
    this.item.no = this.productmanagements.length +1; 
    this.item.proGroup= it.proGroup;
    this.item.proType= it.proType;

    this.fOK= {flag:2, item: this.item };
    this.ref.close(this.fOK);
  }
  hideDialog()
  {
    this.fOK= {flag:0, item: null };

    this.ref.close(this.fOK);
    
  }

}
