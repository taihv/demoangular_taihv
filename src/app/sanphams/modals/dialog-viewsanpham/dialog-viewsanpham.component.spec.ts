import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogViewsanphamComponent } from './dialog-viewsanpham.component';

describe('DialogViewsanphamComponent', () => {
  let component: DialogViewsanphamComponent;
  let fixture: ComponentFixture<DialogViewsanphamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogViewsanphamComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DialogViewsanphamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
