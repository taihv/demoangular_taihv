import { Component, OnInit } from '@angular/core';
import { Customer, Productmanagement, Producttype, Productgroup, Representative } from 'src/app/demo/api/customer';
import { Product } from 'src/app/demo/api/product';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { debug } from 'console';

@Component({
  selector: 'app-dialog-viewsanpham',
  templateUrl: './dialog-viewsanpham.component.html',
  styleUrls: ['./dialog-viewsanpham.component.scss']
})
export class DialogViewsanphamComponent implements OnInit {

  public productmanagements: Productmanagement[] = [];
  public item: Productmanagement = {proGroup:{name: '', code:''}, proType: {name: '', code:''}};
  public producttypes: Producttype[]= [];
  public productgroups: Productgroup[]= [];
  submitted: boolean = false;


  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  ngOnInit(): void {
    debugger;
    this.productmanagements =this.config.data.productmanagements;
    this.producttypes =this.config.data.producttypes;
    this.productgroups =this.config.data.productgroups;
    this.item =this.config.data.item;

  }
  saveProduct()
  {
    // this.item.no = this.productmanagements.length +1;
    this.item.proGroup_name = this.item.proGroup.name;
    this.item.proType_name = this.item.proType.name;

    // this.productmanagements.push(this.item);
    this.ref.close(this.item);
  }
  hideDialog()
  {
    this.ref.close();
    
  }

}
