import { Component } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import {Router} from '@angular/router';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {

    constructor(private primengConfig: PrimeNGConfig, private router: Router) { }

    ngOnInit() {
          this.primengConfig.ripple = true;
          // Navigate to the login page with extras
          this.router.navigate(['/auth/login']);
    }
}
