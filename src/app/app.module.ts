import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppLayoutModule } from './layout/app.layout.module';
import { NotfoundComponent } from './demo/components/notfound/notfound.component';
import { ProductService } from './demo/service/product.service';
import { CountryService } from './demo/service/country.service';
import { CustomerService } from './demo/service/customer.service';
import { EventService } from './demo/service/event.service';
import { IconService } from './demo/service/icon.service';
import { NodeService } from './demo/service/node.service';
import { PhotoService } from './demo/service/photo.service';


import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { RippleModule } from 'primeng/ripple';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { ProgressBarModule } from 'primeng/progressbar';
import { ToastModule } from 'primeng/toast';
import { SliderModule } from 'primeng/slider';
import { RatingModule } from 'primeng/rating';
import {PaginatorModule} from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputNumberModule } from 'primeng/inputnumber';
import {DynamicDialogModule} from 'primeng/dynamicdialog';

import { CheckboxModule } from 'primeng/checkbox';
import { PasswordModule } from 'primeng/password';



import { SanphamsComponent } from './sanphams/sanphams.component';
import { LoaisanphamsComponent } from './loaisanphams/loaisanphams.component';
import { NhomsanphamsComponent } from './nhomsanphams/nhomsanphams.component';
import { DialogEditsanphamComponent } from './sanphams/modals/dialog-editsanpham/dialog-editsanpham.component';
import { DialogAddsanphamComponent } from './sanphams/modals/dialog-addsanpham/dialog-addsanpham.component';
import { DialogViewsanphamComponent } from './sanphams/modals/dialog-viewsanpham/dialog-viewsanpham.component';
import { DialogDelmessageComponent } from './showmodals/dialog-delmessage/dialog-delmessage.component';


@NgModule({
    declarations: [
        AppComponent, NotfoundComponent, SanphamsComponent, LoaisanphamsComponent, NhomsanphamsComponent, DialogEditsanphamComponent, DialogAddsanphamComponent, DialogViewsanphamComponent, DialogDelmessageComponent, 
    ],
    imports: [
        AppRoutingModule,
        AppLayoutModule,
        BrowserModule,
        CommonModule,
        FormsModule,
        TableModule,
        RatingModule,
        ButtonModule,
        SliderModule,
        InputTextModule,
        InputTextareaModule,
        RadioButtonModule,
        InputNumberModule,
        RatingModule,
        ToggleButtonModule,
        RippleModule,
        PaginatorModule,
        MultiSelectModule,
        DropdownModule,
        ProgressBarModule,
        ToastModule,
        DialogModule,
        DynamicDialogModule,
        CheckboxModule,
        PasswordModule

    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        CountryService, CustomerService, EventService, IconService, NodeService,
        PhotoService, ProductService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
