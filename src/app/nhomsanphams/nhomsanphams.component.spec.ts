import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NhomsanphamsComponent } from './nhomsanphams.component';

describe('NhomsanphamsComponent', () => {
  let component: NhomsanphamsComponent;
  let fixture: ComponentFixture<NhomsanphamsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NhomsanphamsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NhomsanphamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
