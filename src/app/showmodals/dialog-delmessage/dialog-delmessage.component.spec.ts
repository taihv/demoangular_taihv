import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDelmessageComponent } from './dialog-delmessage.component';

describe('DialogDelmessageComponent', () => {
  let component: DialogDelmessageComponent;
  let fixture: ComponentFixture<DialogDelmessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogDelmessageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DialogDelmessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
