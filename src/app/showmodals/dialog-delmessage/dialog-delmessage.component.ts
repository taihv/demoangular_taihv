import { Component, OnInit } from '@angular/core';
import { Message } from 'src/app/demo/api/customer';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { debug } from 'console';

@Component({
  selector: 'app-dialog-delmessage',
  templateUrl: './dialog-delmessage.component.html',
  styleUrls: ['./dialog-delmessage.component.scss']
})
export class DialogDelmessageComponent implements OnInit {

  
  public item: Message = {};


  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  ngOnInit(): void {
    
    this.item =this.config.data.item;

  }
  deleteProductsDialog()
  {
    this.ref.close(this.item);
  }
  confirmDeleteSelected()
  {
    this.ref.close();
    
  }

}
