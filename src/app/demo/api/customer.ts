export interface Country {
    name?: string;
    code?: string;
}

export interface Representative {
    name?: string;
    image?: string;
}

export interface Customer {
    id?: number;
    name?: string;
    country?: Country;
    company?: string;
    date?: string;
    status?: string;
    activity?: number;
    representative?: Representative;
}

export interface Message {
    
    title?: string,
    message?: string
    name?: string
  }



export interface Producttype {
    id?: number;
    name: string,
    code?: string
  }

  export interface Productgroup {
    id?: number;
    name: string,
    code?: string
  }
export interface Productmanagement {
    id?: number;
    no?: number;
    product_lotcode?: string;
    proGroup: Productgroup;
    proGroup_id?: string;
    proGroup_name?: string;
    
    proType: Producttype;

    proType_id?: string;
    proType_name?: string;
    product_name?: string;
    material?: string;
    weight?: number;
    capacity?: number;
    description?: string;
    unit_product?: string;
    product_dom ?: string;
    product_expiry?: number;
    
}


